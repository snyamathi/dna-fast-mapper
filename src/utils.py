import random
import itertools

bases = ['A', 'C', 'G', 'T']
sqrt_n = 10000

def getIterationsOfBasesAsDict():
    iterations = itertools.product(bases, repeat=8)
    dict = {}
    for i in iterations:
        dict[''.join(i)] = 0
    return dict

def count(filepath):
    dict = getIterationsOfBasesAsDict()
    f = open(filepath)
    current_bases = []
    current_bases.append(f.read(1))
    current_bases.append(f.read(1))
    current_bases.append(f.read(1))
    current_bases.append(f.read(1))
    current_bases.append(f.read(1))
    current_bases.append(f.read(1))
    current_bases.append(f.read(1))
    while True:
        c = f.read(1)
        if not c:
            print 'EOF'
            break
        current_bases.append(c)
        dict[''.join(current_bases)] += 1
        current_bases = current_bases[1:]
    f.close()
    return dict

def writeCountsToFile(filepath, dict):
    keys = dict.keys()
    keys.sort()    
    with open(filepath, 'w+') as f:
        for key in keys:
            f.write(str(key))
            f.write(',')
            f.write(str(dict[key]))
            f.write('\n')
        

def writeRandomGenome(filepath):
    current_percent = 0
    with open(filepath, 'w+') as f:
        for i in range(sqrt_n):
            print sqrt_n - i
            for j in range(sqrt_n):
				f.write(bases[random.randint(0, 3)])
