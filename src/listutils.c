#include "listutils.h"

void getModeOfList(int* list, int* mode, int* modeCount)
{
    *mode = 0;
    *modeCount = 0;
    
    int i;
    int current = 0;
    int currentCount = 0;
    for (i = 0; i < list[0]; ++i) {
        if (list[i+1] == current) {
            ++currentCount;
        } else {
            current = list[i+1];
            currentCount = 1;
        }

        if (currentCount > *modeCount) {
            *mode = current;
            *modeCount = currentCount;
        }
    }
}

void mergeWithOffset(int* masterList, int** remainingLists, int sequenceLength)
{
    int i;
    int j;
    int smallestNumber;
    int smallestNumberLocation;
    
    int* indexes;
    indexes = (int*) malloc(sequenceLength * sizeof(int));
    for (i = 0; i < sequenceLength; ++i) {
        indexes[i] = 0;
    }
    
    for (i = 0; i < masterList[0]; ++i) {
        smallestNumber = INT_MAX;
        smallestNumberLocation = -1;
        for (j = 0; j < sequenceLength; ++j) {
            if (remainingLists[j][indexes[j]+1] - j <= smallestNumber && indexes[j] < remainingLists[j][0]) {
                smallestNumber = remainingLists[j][indexes[j]+1] - j;
                smallestNumberLocation = j;
            }
        }
        masterList[i+1] = smallestNumber;
        ++indexes[smallestNumberLocation];
    }
    
    // TODO: Free memory
    free(indexes);
}

void printList(int* list)
{
    int i;
    for (i = 0; i < list[0]; ++i) {
        printf("list[%i] = %i\n", i+1, list[i+1]);
    }
}

void printLists(int** lists, int sequenceLength)
{
    int i;
    int j;
    for (i = 0; i < sequenceLength; ++i) {
        for (j = 0; j < lists[i][0]; ++j) {
            printf("[%i][%i] = %i\n", i, j+1, lists[i][j+1]);
        }
    } 
}

int* getMasterList(int** lists, int sequenceLength)
{
    int i;
    int count = 0;
    for (i = 0; i < sequenceLength; ++i) {
        count += lists[i][0];
    }
    
    int* masterList;
    masterList = (int*) malloc((count+1) * sizeof(int));
    masterList[0] = count;
    return masterList;
}
