#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

int* getMasterList(int** lists, int sequenceLength);

void printLists(int** lists, int sequenceLength);

void printList(int* list);

int** getLists();

void mergeWithOffset(int* masterList, int** remainingLists, int sequenceLength);

void getModeOfList(int* list, int* mode, int* modeCount);