#include <stdio.h>
#include <stdlib.h>

unsigned short basesToInt(int length, short value, char* sequence);

void createBuckets(int** lookupTable, const char* countsFilePath);

void populateBuckets(int** lookupTable, const char* genomeFilePath);

int** getListsFromSequence(int** lookupTable, char* sequence, int sequenceLength);