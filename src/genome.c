#include "genome.h"

int** getListsFromSequence(int** lookupTable, char* sequence, int sequenceLength)
{
    int i;
    int** lists = (int**) malloc(sequenceLength * sizeof(int*));
    for (i = 0; i < sequenceLength; ++i) {
        lists[i] = lookupTable[basesToInt(8, 0, sequence + i)];
    }
    return lists;
}

unsigned short basesToInt(int length, short value, char* sequence)
{
	int i;
	for (i = 0; i < length; ++i) {
		value <<= 2;
		switch(sequence[i]) {
            case 'A':
                // value += 0;
                break;
            case 'C':
                value += 1;
                break;
            case 'G':
                value += 2;
                break;
            case 'T':
                value += 3;
                break;
		}
	}
	return value;
}

void createBuckets(int** lookupTable, const char* countsFilePath)
{
	FILE *countsFile;
	countsFile = fopen(countsFilePath, "r");
	if (countsFile == NULL) {
		// file not present
	}
    
	int countForBucket = 0;
	int i;
	for (i = 0; i < 65536; ++i) {
		fscanf(countsFile, "%*9s%i", &countForBucket);
		lookupTable[i] = (int*) malloc((countForBucket+1) * sizeof(int));
		if (lookupTable[i]) {
			lookupTable[i][0] = countForBucket;
		} else {
			printf("ERR: malloc failed to populate bucket %i\n", i);
		}
	}
}

void populateBuckets(int** lookupTable, const char* genomeFilePath)
{
	FILE *genomeFile;
	genomeFile = fopen(genomeFilePath, "r");
	if (genomeFile == NULL) {
        printf("ERR: Genome file not found at %s\n", genomeFilePath);
        exit(1);
	}
    
	char bases[7];
	unsigned short key = 0;
	unsigned int index = 0;
    
    int i;
    int lookupTableIndexes[65536];
    for (i = 0; i < 65536; ++i) {
        lookupTableIndexes[i] = 0;
    }
    
	// prime the octet and key
	fread(bases, 1, 7, genomeFile);
	key = basesToInt(7, 0, bases);

	while (fread(bases, 1, 1, genomeFile) == 1) {
		key = basesToInt(1, key, bases);
		lookupTable[key][lookupTableIndexes[key]+1] = index;
		++lookupTableIndexes[key];
		++index;
	}
}
