#include <stdio.h>
#include <stdlib.h>
#include "listutils.h"
#include "genome.h"

void findSequence(int** lookupTable, char* sequence, int sequenceLength, int* mode, int* modeCount)
{
    int** lists;
    int* masterList;

    lists = getListsFromSequence(lookupTable, sequence, sequenceLength);
    masterList = getMasterList(lists, sequenceLength);

    mergeWithOffset(masterList, lists, sequenceLength);
    getModeOfList(masterList, mode, modeCount);
}

void printSequence(char* refSequence, char* inputSequence, int sequenceLength)
{
	int i;
	char diff[sequenceLength+1];
	diff[sequenceLength] = '\0';
	for (i = 0; i < sequenceLength; ++i) {
		diff[i] = refSequence[i] == inputSequence[i] ? ' ' : '^';
	}
	printf("Reference  %s\n", refSequence);
	printf("Input      %s\n", inputSequence);
	printf("           %s\n", diff);
}

void getReferenceSequence(char* refSequence, int sequenceLength, int offset)
{
	FILE* referenceFile;
	referenceFile = fopen("H:\\short_genome.txt", "r");

	refSequence[sequenceLength] = '\0';
	fseek(referenceFile, offset, SEEK_SET);
	fread(refSequence, 1, sequenceLength, referenceFile);
	fclose(referenceFile);
}

void getInputSequence(char* sequence, int* sequenceLength)
{
    printf("Enter a sequence:\n>");
    fseek(stdin,0,SEEK_END);
    scanf("%s%n", sequence, sequenceLength);
    sequence[*sequenceLength] = '\0';
}

void demo()
{
	// Create the lookup table
	int** lookupTable;
	lookupTable = (int**) malloc(65536 * sizeof(int*));

	// Allocate space for each of the table entries which are
	// arrays themselves as we have a lot of collisions
	// createBuckets(lookupTable, "/Volumes/Storage/genome/short_counts.txt");
	// populateBuckets(lookupTable, "/Volumes/Storage/genome/short_genome.txt");

	createBuckets(lookupTable, "H:\\short_counts.txt");
	populateBuckets(lookupTable, "H:\\short_genome.txt");

	char sequence[100];
	char refSequence[100];
	int sequenceLength;
	int mode;
	int modeCount;
    while (1) {
    	mode = 0;
    	modeCount = 0;
    	sequenceLength = 0;

        getInputSequence(sequence, &sequenceLength);

        printf("\nSearching for sequence %s (%i bases)\n", sequence, sequenceLength);
        sequenceLength -= 8;

        findSequence(lookupTable, sequence, sequenceLength, &mode, &modeCount);
        getReferenceSequence(refSequence, sequenceLength+8, mode);

        printf("Sequence found at position %i with match %i of %i (%f\%%)\n\n", mode, modeCount, sequenceLength, ((float) modeCount / (float) sequenceLength)*100);
        printSequence(refSequence, sequence, sequenceLength);
    }
}

int main(void)
{
	demo();
	return 0;
}
